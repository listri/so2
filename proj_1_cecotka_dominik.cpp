#include <ncurses.h>
#include <unistd.h>
#include <iostream>
#include <thread>

int rows;
int columns;
int end_thread = -1;

void first_thread() {
	int column_offset = 0;

	while (true) {
		if (end_thread == 'q') {
			break;
		}
		while (column_offset != columns) {
			column_offset++;
			mvprintw(0, column_offset, "1");
			mvprintw(0, column_offset - 1, " ");
			refresh();
			if (end_thread == 'q') {
				break;
			}
			usleep(100000);
		}
		while (column_offset >= 1) {
			column_offset--;
			mvprintw(0, column_offset, "1");
			mvprintw(0, column_offset + 1, " ");
			refresh();
			if (end_thread == 'q') {
				break;
			}
			usleep(100000);
		}
	}
}

void second_thread() {
	int column_offset = 0;

	while (true) {
		if (end_thread == 'q') {
			break;
		}
		while (column_offset != columns) {
			column_offset++;
			mvprintw(1, column_offset, "2");
			mvprintw(1, column_offset - 1, " ");
			refresh();
			if (end_thread == 'q') {
				break;
			}
			usleep(75000);
		}
		while (column_offset >= 1) {
			column_offset--;
			mvprintw(1, column_offset, "2");
			mvprintw(1, column_offset + 1, " ");
			refresh();
			if (end_thread == 'q') {
				break;
			}
			usleep(75000);
		}
	}
}

void listener() {
	end_thread = getch();
}

int main() {
	initscr();
	noecho();
	getmaxyx(stdscr, rows, columns);

	std::thread t1{first_thread};
	std::thread t2{second_thread};
	std::thread t3{listener};

	t1.join();
	t2.join();
	t3.join();

	endwin();
	return 0;
}
